; Use this file to build a full distribution including Drupal core and the
; AnotherDrop Startup install profile using the following command:
;
; drush make anotherdrop_startup.make <target directory>

api = 2
core = 7.x

; Include the definition for how to build Drupal core directly, including patches:
includes[] = drupal-org-core.make


; Download the install profile and recursively build all its dependencies:
;projects[anotherdrop_startup][version] = 7.x-1.x

; Add Opendeals to the full distribution build.
projects[anotherdrop_startup][type] = profile
;projects[anotherdrop_startup][version] = 1.x-dev
projects[anotherdrop_startup][download][type] = git
projects[anotherdrop_startup][download][url] = http://git.drupal.org:sandbox/hsimeoni/1846292.git
projects[anotherdrop_startup][download][branch] = 7.x-1.x
